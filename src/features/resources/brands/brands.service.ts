import { HttpException, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { ItemsService } from "../items/items.service";
import { Brand, BrandDocument } from "./brand.schema";
import { CreateBrandDto } from "./dto/create-brand.dto";
import { UpdateBrandDto } from "./dto/update-brand.dto";

@Injectable()
export class BrandsService {
    constructor(
        @InjectModel(Brand.name) private brandModel: Model<BrandDocument>,
        private itemsService: ItemsService
    ) { }

    async create(createBrandDto: CreateBrandDto): Promise<Brand> {
        const createdBrand = new this.brandModel(createBrandDto);
        return createdBrand.save();
    }

    async update(updateBrandDto: UpdateBrandDto): Promise<Brand> {
        const brand = await this.brandModel.findById(updateBrandDto._id);
        brand.name = updateBrandDto.name;
        brand.shortname = updateBrandDto.shortname;
        brand.shop = updateBrandDto.shop;
        this.itemsService.bulkWrite([{
            updateMany: {
                filter: { 'brand._id': { $eq: updateBrandDto._id } },
                update: {
                    $set: {
                        'brand.name': brand.name,
                        'brand.shortname': brand.shortname,
                        'brand.shop': brand.shop,
                    }
                }
            }
        }]);
        return brand.save();
    }

    async insertMany(items: CreateBrandDto[]): Promise<Brand[]> {
        return this.brandModel.insertMany(items);
    }

    async findAll(): Promise<Brand[]> {
        return this.brandModel.find().sort({ name: 'asc' }).exec();
    }

    async delete(id: string): Promise<any> {
        const brand = this.brandModel.findById(id);
        if (brand) {
            return brand.deleteOne().exec();
        }
        throw new HttpException('There is no item to delete for the given id', 410);
    }

    /**
     * Get the total size of the collection (storage size and index size) in byte.
     */
    async totalSize(): Promise<number> {
        const stats = await this.brandModel.collection.stats();
        return stats.storageSize + stats.totalIndexSize;
    }
}