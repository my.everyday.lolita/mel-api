export interface Criterium {
    type: 'brand' | 'color' | 'feature' | 'category' | 'keyword' | 'own' | 'id' | 'drafts' | 'mel-error' | 'incomplete' | 'user';
    value: string;
    parents?: string[];
}