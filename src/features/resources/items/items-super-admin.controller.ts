import { Controller, Get, Query, SetMetadata, Sse, UseGuards } from "@nestjs/common";
import { catchError, concat, delay, map, Observable, of, onErrorResumeNext } from "rxjs";
import { AuthGuard } from "src/features/keycloak/guards/auth.guard";
import { RolesGuard } from "src/features/keycloak/guards/roles.guard";
import { ItemsService } from "./items.service";

@Controller('/api/super-admin/resources/items')
export class ItemsSuperAdminController {

    constructor(private itemsService: ItemsService) { }

    @Get('check-images')
    @UseGuards(AuthGuard, RolesGuard)
    @SetMetadata('roles', ['super-admin'])
    async checkImages(@Query('limit') limit: string) {
        const images = await this.itemsService.getNonMelImages(parseInt(limit, 10));
        return images.length;
    }

    @Get('check-unused-images')
    @UseGuards(AuthGuard, RolesGuard)
    @SetMetadata('roles', ['super-admin'])
    async checkUnusedImages() {
        const images = await this.itemsService.getUnusedImages();
        return images.length;
    }

    @Sse('download-images')
    @UseGuards(AuthGuard, RolesGuard)
    @SetMetadata('roles', ['super-admin'])
    async downloadImages(@Query('limit') limit: string, @Query('delay') delayBetweenRequests: string): Promise<Observable<any>> {
        const imagesToDownload$ = await this.itemsService.downloadImages(parseInt(limit, 10));
        const total = imagesToDownload$.length;
        const _delayBetweenRequests = Math.max(100, parseInt(delayBetweenRequests, 10));
        return onErrorResumeNext(...imagesToDownload$.map(obs => obs.pipe(
            catchError(err => {
                return of({ total, step: -1 });
            })
        ))).pipe(
            delay(_delayBetweenRequests),
            map((value, index) => {
                return { data: { ...value, total, step: index + 1 }, id: index + 1 };
            }),
        );
    }

    @Sse('remove-unused-images')
    @UseGuards(AuthGuard, RolesGuard)
    @SetMetadata('roles', ['super-admin'])
    async removeUnusedImages(): Promise<Observable<any>> {
        const imagesToRemove$ = await this.itemsService.removeUnusedImages();
        const total = imagesToRemove$.length;
        return concat(...imagesToRemove$.map(obs => obs.pipe(delay(100)))).pipe(
            map((_, index) => {
                return { data: { total, step: index + 1 }, id: index + 1 };
            }),
        );
    }

    @Sse('images-size')
    @UseGuards(AuthGuard, RolesGuard)
    @SetMetadata('roles', ['super-admin'])
    async imagesSize(): Promise<Observable<any>> {
        const imagesSize$ = await this.itemsService.getImagesSize();
        const total = imagesSize$.length;
        return concat(...imagesSize$.map(obs => obs.pipe(delay(50)))).pipe(
            map((size, index) => {
                return { data: { size, total, step: index + 1 }, id: index + 1 };
            }),
        );
    }

}