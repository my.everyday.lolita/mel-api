import { HttpService } from "@nestjs/axios";
import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { InjectModel } from "@nestjs/mongoose";
import { createWriteStream, readdirSync, renameSync, statSync, unlinkSync, writeFileSync } from "fs";
import { Model, Query } from "mongoose";
import { basename, extname, join } from "path";
import { from, iif, lastValueFrom, Observable, of, throwError, zip, concat } from "rxjs";
import { catchError, map, switchMap } from "rxjs/operators";
import { ENV } from "src/features/environement/env.model";
import { UserHelper } from "src/features/keycloak/decorators/user-helper.decorator";
import { AppLogger } from "src/features/logs/app.logger";
import { TinifyService } from "src/features/tinify/tinify.service";
import { Feature } from "../features/feature.schema";
import { CreateItemDto } from "./dto/create-item.dto";
import { Criterium } from "./dto/criterium.dto";
import { UpdateItemDto } from "./dto/update-item.dto";
import { Item, ItemDocument, ItemStatus, ItemVariant } from "./item.schema";
import { v4 as uuidv4 } from 'uuid';

export interface NonMELImage {
    image: string;
    colors: any[];
    _id: string;
    isImage: boolean;
}

export type NonMelImageType = 'http' | 'binary';

export interface PreparedNonMelImage {
    currentValue: string;
    newValue: string;
    itemId: string;
    type: NonMelImageType;
}

export interface DownloadedNonMelImage {
    old: string;
    new: string;
    item: string;
    size?: number;
    imageType?: string;
}

@Injectable()
export class ItemsService {

    private tmpImages: { [key: string]: number } = {};

    constructor(
        @InjectModel(Item.name) private itemModel: Model<ItemDocument>,
        private http: HttpService,
        private configService: ConfigService,
        private tinify: TinifyService,
        private logger: AppLogger
    ) { }

    async findById(id: string, userHelper: UserHelper): Promise<Item> {
        const item = await this.itemModel.findById(id);
        if (item.status === ItemStatus.DRAFT) {
            if (!userHelper.isAdmin && item.owner !== userHelper.sub) {
                this.logger.error(`User ${userHelper.sub} tried to access item ${id}`, JSON.stringify(userHelper));
                throw new HttpException('Access denied', HttpStatus.FORBIDDEN);
            }
        }
        if (item.draft !== undefined) {
            if (!userHelper.isAdmin && item.owner !== userHelper.sub) {
                item.draft = undefined;
            }
        }
        return item;
    }

    async create(createItemDto: CreateItemDto, userHelper: UserHelper): Promise<Item> {
        createItemDto.created = new Date();
        createItemDto.modified = new Date();
        const createdItem = new this.itemModel(createItemDto);
        createdItem.status = !userHelper.isAdmin ? ItemStatus.DRAFT : ItemStatus.VALIDATED;
        const item = await createdItem.save();
        await this.handleTmpImages(item);
        return item;
    }

    async update(updateItemDto: UpdateItemDto, userHelper: UserHelper): Promise<Item> {
        const item = await this.itemModel.findById(updateItemDto._id);
        if (userHelper.isAdmin) {
            item.brand = updateItemDto.brand;
            item.category = updateItemDto.category;
            item.collectionn = updateItemDto.collectionn;
            item.features = updateItemDto.features as [Feature];
            item.variants = updateItemDto.variants as [ItemVariant];
            item.keywords = updateItemDto.keywords as [string];
            item.substyles = updateItemDto.substyles as [string];
            item.japanese = updateItemDto.japanese;
            item.year = updateItemDto.year;
            item.measurments = updateItemDto.measurments;
            item.estimatedPrice = updateItemDto.estimatedPrice;
            item.modified = new Date();
            item.status = updateItemDto.status;
            item.draft = undefined;
            item.incomplete = updateItemDto.incomplete;
        } else if (userHelper.isOwn) {
            item.draft = updateItemDto;
        } else {
            throw new HttpException('You are not allowed to update this item', HttpStatus.FORBIDDEN);
        }
        await item.save();
        await this.handleTmpImages(item);
        return item;
    }

    async insertMany(items: CreateItemDto[]): Promise<Item[]> {
        return this.itemModel.insertMany(items);
    }

    async delete(id: string): Promise<any> {
        const item = this.itemModel.findById(id);
        if (item) {
            return item.deleteOne().exec();
        }
        throw new HttpException('There is no item to delete for the given id', 410);
    }

    async findAll(): Promise<Item[]> {
        return this.itemModel.find().exec();
    }

    async bulkWrite(writes: any[]) {
        return this.itemModel.bulkWrite(writes);
    }

    findByCriteria(criteria: Criterium[], skip: number, userHelper: UserHelper, limit = 20, bypassLimit = false): Query<ItemDocument[], ItemDocument> {
        let filter = this.getFindDefaultFilter(userHelper);
        const projection: any = { draft: false };
        if (userHelper.isAdmin) {
            filter.$and = [];
            delete projection.draft;
        }
        const sort: any = {
            created: -1,
        };
        const keywords = criteria.filter(crit => crit.type === 'keyword');
        if (keywords.length > 0) {
            filter.$and.push({
                $text: { $search: keywords.map(crit => crit.value).join(' ') }
            });
            projection.score = { '$meta': 'textScore' };
            delete sort.created;
            sort.score = { '$meta': 'textScore' };
        }
        const brands = criteria.filter(crit => crit.type === 'brand');
        if (brands.length > 0) {
            filter.$and.push({
                'brand.name': { $in: brands.map(crit => crit.value) }
            });
        }
        const categories = criteria.filter(crit => crit.type === 'category');
        if (categories.length > 0) {
            const categoryNames = categories.map(crit => crit.value);
            const $categoryOr = [];
            $categoryOr.push({
                'category.name': { $in: categoryNames }
            });
            $categoryOr.push({
                'category.parent.name': { $in: categoryNames }
            });
            $categoryOr.push({
                'category.parent.parent.name': { $in: categoryNames }
            });
            filter.$and.push({ $or: $categoryOr })
        }
        const colors = criteria.filter(crit => crit.type === 'color');
        if (colors.length > 0) {
            filter.$and.push({
                'variants.colors.name': { $all: colors.map(crit => crit.value) }
            });
        }
        const features = criteria.filter(crit => crit.type === 'feature');
        if (features.length > 0) {
            filter.$and.push({
                'features.name': { $all: features.map(crit => crit.value) }
            });
        }
        const own = criteria.filter(crit => crit.type === 'own');
        if (own.length > 0) {
            bypassLimit = true;
            const email = own[0].value;
            filter.$and.push({
                owner: email
            });
        }
        const drafts = criteria.filter(crit => crit.type === 'drafts');
        if (drafts.length > 0 && userHelper.isAdmin) {
            filter.$and.push({
                $or: [
                    { status: { $eq: ItemStatus.DRAFT } },
                    { draft: { $exists: true } },
                ]

            })
        }
        const byIds = criteria.filter(crit => crit.type === 'id');
        if (byIds.length > 0) {
            bypassLimit = true;
            filter.$and.push({
                _id: { $in: byIds.map(crit => crit.value) }
            });
        }
        const melErrors = criteria.filter(crit => crit.type === 'mel-error');
        if (melErrors.length > 0 && userHelper.isAdmin) {
            bypassLimit = true;
            filter.$and.push({
                'variants.photos': { $regex: 'mel-error' }
            });
        }
        const incomplete = criteria.filter(crit => crit.type === 'incomplete');
        if (incomplete.length > 0 && userHelper.isAdmin) {
            bypassLimit = true;
            filter.$and.push({
                'incomplete': { $eq: true }
            });
        }
        const users = criteria.filter(crit => crit.type === 'user');
        if (users.length > 0 && userHelper.isAdmin) {
            bypassLimit = true;
            filter.$and.push({
                'owner': { $in: users.map(crit => crit.value) }
            });
        }

        if (filter.$and.length === 0) {
            delete filter.$and;
        }
        let query = this.itemModel.find(filter, projection);
        if (!bypassLimit) {
            query = query.limit(Math.min(limit, 500)).skip(skip);
        }
        return query.sort(sort);
    }

    async recentlyAdded(userHelper: UserHelper): Promise<Item[]> {
        const filter = this.getFindDefaultFilter(userHelper);
        const projection: any = { draft: false };
        if (userHelper.isAdmin) {
            delete filter.$and;
            delete projection.draft;
        }
        return this.itemModel.find(filter, projection).sort({ created: -1 }).limit(20).exec();
    }

    async stats(): Promise<any> {
        return this.itemModel.aggregate([
            {
                $facet: {
                    "brands": [
                        { $unwind: "$brand" },
                        { $sortByCount: "$brand" },
                    ],
                    "categories": [
                        { $unwind: "$category.name" },
                        { $sortByCount: "$category.name" },
                    ],
                    "colors": [
                        { $unwind: "$variants" },
                        { $unwind: "$variants.colors" },
                        { $sortByCount: "$variants.colors" },
                    ],
                    "features": [
                        { $unwind: "$features" },
                        { $sortByCount: "$features" },
                    ],
                    "count_items": [
                        { $count: "total" }
                    ],
                    "count_variants": [
                        { $unwind: "$variants" },
                        { $count: "total" }
                    ],
                },
            },
        ]).exec();
    }

    async countDrafts(): Promise<number> {
        const result = await this.itemModel.aggregate([
            {
                $facet: {
                    "count_drafts": [
                        {
                            $match: {
                                $or: [
                                    { status: { $eq: ItemStatus.DRAFT } },
                                    { draft: { $exists: true } }
                                ]
                            }
                        },
                        { $count: "drafts" }
                    ],
                },
            },
        ]).exec();
        return result[0] && result[0].count_drafts[0] && result[0].count_drafts[0].drafts || 0;
    }

    /**
     * Get the total size of the collection (storage size and index size) in byte.
     */
    async totalSize(): Promise<number> {
        const stats = await this.itemModel.collection.stats();
        return stats.storageSize + stats.totalIndexSize;
    }

    private getFindDefaultFilter(userHelper: UserHelper): any {
        const filter = {
            $and: []
        };
        if (userHelper.isAnonymous) {
            const $or = [
                { status: { $eq: ItemStatus.VALIDATED } },
                { status: { $exists: false } }
            ];
            filter.$and.push({ $or });
        } else {
            if (!userHelper.isAdmin) {
                const $or = [
                    { status: { $not: { $eq: ItemStatus.DRAFT } } },
                    {
                        status: { $eq: ItemStatus.DRAFT },
                        owner: { $eq: userHelper.sub }
                    },
                    { status: { $exists: false } },
                ];
                filter.$and.push({ $or });
            }
        }
        return filter;
    }

    /**
     * Retrieve non MEL images from the database
     * A non MEL image is an image that not contain the API domain name.
     */
    async getNonMelImages(limit: number): Promise<NonMELImage[]> {
        const result: { images: NonMELImage[] }[] = await this.itemModel.aggregate([
            {
                $facet: {
                    'images': [
                        { $unwind: "$variants" },
                        { $unwind: { path: "$variants.photos", includeArrayIndex: 'imageIndex' } },
                        {
                            $project: {
                                image: '$variants.photos',
                                colors: '$variants.colors'
                            }
                        },
                        { $match: { image: { $not: { $regex: new RegExp(`(^${this.configService.get(ENV.BASE_URL)}|mel-error=)`) } } } },
                        { $limit: Math.min(limit, 500) },
                    ]
                }
            },
        ]).exec();
        if (result[0].images.length > 0) {
            return result[0].images;
            // const isImages$ = result[0].images.map(obj => {
            //     if (obj.image.startsWith('data:image/')) {
            //         obj.isImage = true;
            //         return of(obj);
            //     }
            //     return this.http.head(obj.image).pipe(
            //         map(res => {
            //             if (!res.headers['content-type']) {
            //                 obj.isImage = /\.(jpeg|jpg|png|gif|webp)/.test(obj.image);
            //             } else {
            //                 obj.isImage = res.headers['content-type'] && res.headers['content-type'].startsWith('image/');
            //             }
            //             return obj;
            //         }),
            //         catchError((err) => {
            //             console.error(err);
            //             return of(undefined);
            //         })
            //     );
            // });
            // const isImages = await lastValueFrom(concat(...isImages$));
            // return result[0].images.filter(obj => {
            //     const o = isImages.find(o => o === obj);
            //     return o && o.isImage;
            // });
        }
        return [];
    }

    intermediateNonMelImageProcess(images: NonMELImage[]): PreparedNonMelImage[] {
        return images.map(image => {
            const newFilename = `${image._id}_${uuidv4()}`;
            let type: NonMelImageType = 'http';
            let extension: string;
            let currentValue = image.image;
            if (image.image.startsWith('http') && !image.image.startsWith(this.configService.get(ENV.BASE_URL))) {
                const filename = basename((new URL(image.image)).pathname);
                extension = extname(filename);
            } else if (image.image.startsWith('data:image/')) {
                const match = /^data:image\/(.*);base64,(.+)/.exec(image.image);
                if (match[1] !== undefined) {
                    extension = `.${match[1]}`;
                }
                type = 'binary';
            }
            return {
                currentValue,
                newValue: `${newFilename}${extension || '.jpg'}`,
                itemId: image._id,
                type,
            }
        });
    }

    async downloadImages(limit = 500): Promise<Observable<DownloadedNonMelImage>[]> {
        const images = await this.getNonMelImages(limit);
        return this.intermediateNonMelImageProcess(images).map(obj => {
            let result: Observable<DownloadedNonMelImage> = undefined;
            switch (obj.type) {
                case 'http':
                    result = this.http.get(obj.currentValue, {
                        responseType: 'stream',
                        timeout: 10000,
                    }).pipe(
                        switchMap(response => {
                            const type = response.headers['content-type'];
                            if (type && !type.startsWith('image/')) {
                                throw new HttpException('Invalid image type', HttpStatus.UNSUPPORTED_MEDIA_TYPE);
                            }
                            const data = { data: response.data, name: obj.newValue };
                            const file = createWriteStream(join(this.configService.get(ENV.ASSETS_PATH), data.name));
                            const result = from(new Promise<void>((resolve, reject) => {
                                file.on('finish', () => resolve());
                                file.on('error', () => reject());
                            }))
                            response.data.pipe(file);
                            const size = response.headers['content-length'];
                            let imageType: string;
                            if (!response.headers['content-type']) {
                                imageType = obj.currentValue.match(/\.(jpeg|jpg|png|gif|webp)/)[0] || 'jpg';
                            } else {
                                imageType = response.headers['content-type'].split('/')[1];
                            }
                            return result.pipe(map(_ => ({ old: obj.currentValue, new: data.name, item: obj.itemId, size, imageType })));
                        }),
                        catchError(err => {
                            return this.updateNonMelImageOnError(obj, err).pipe(
                                switchMap(() => throwError(() => err)),
                            );
                        }),
                    );
                    break;
                case 'binary':
                    result = from(new Promise<void>(resolve => {
                        const match = /^data:image\/(.*);base64,(.+)/.exec(obj.currentValue);
                        writeFileSync(join(this.configService.get(ENV.ASSETS_PATH), obj.newValue), Buffer.from(match[2], `base64`));
                        resolve();
                    })).pipe(
                        map(_ => {
                            const match = /^data:image\/(.*);base64,(.+)/.exec(obj.currentValue);
                            const size = atob(match[2]).length;
                            return { old: obj.currentValue.substring(0, 128), new: obj.newValue, item: obj.itemId, imageType: match[1], size }
                        }),
                    );
                    break;

                default:
                    break;
            }
            return result.pipe(
                switchMap(result => {
                    const filepath = join(this.configService.get(ENV.ASSETS_PATH), result.new);
                    return iif(
                        () => result.size >= 300000 && ['jpeg', 'jpg', 'png'].includes(result.imageType),
                        this.tinify.fromFile(filepath, filepath).pipe(map(() => result)),
                        of(result)
                    );
                }),
                switchMap(result => this.replaceNonMelImage(result)),
                catchError(err => {
                    if (err instanceof Error) {
                        this.logger.error(`${err.message} [${obj.itemId}]`, `${err.stack}\n\n${obj.itemId}\n${obj.currentValue}\n${obj.newValue}`, 'Error image processing');
                    } else if (typeof err === 'string') {
                        this.logger.error(err, 'No trace available', 'Error image processing');
                    }
                    return throwError(() => err);
                })
            );
        }).filter(obs => obs !== undefined);
    }

    replaceNonMelImage(obj: DownloadedNonMelImage): Observable<any> {
        return from(this.itemModel.updateOne(
            {
                'variants.photos': { $eq: obj.old }
            },
            {
                $set: {
                    'variants.$[].photos.$[photo]': `${this.configService.get(ENV.BASE_URL)}/files/${obj.new}`
                },
            },
            {
                arrayFilters: [
                    { 'photo': { $eq: obj.old } }
                ]
            }
        ).exec());
    }

    async getUnusedImages(): Promise<string[]> {
        const files = readdirSync(this.configService.get(ENV.ASSETS_PATH));
        const result = await this.itemModel.aggregate([
            {
                $facet: {
                    'images': [
                        { $unwind: "$variants" },
                        { $unwind: { path: "$variants.photos" } },
                        {
                            $project: {
                                image: '$variants.photos',
                            }
                        },
                    ]
                }
            }
        ]).exec();
        const toRemove = files.filter(filename => !result[0].images.find((o: { image: string }) => o.image.includes(filename)));
        return toRemove;
    }

    async removeUnusedImages(): Promise<Observable<boolean>[]> {
        const imagesToRemove = (await this.getUnusedImages());
        return imagesToRemove.map(image => {
            return new Observable(obs => {
                try {
                    unlinkSync(join(this.configService.get(ENV.ASSETS_PATH), image.replace(`${this.configService.get(ENV.BASE_URL)}/`, '')));
                    obs.next(true);
                } catch (error) {
                    obs.next(false);
                }
                obs.complete();
            });
        });
    }

    async getImagesSize(): Promise<any> {
        const result = await this.itemModel.aggregate([
            {
                $facet: {
                    'images': [
                        { $unwind: "$variants" },
                        { $unwind: { path: "$variants.photos" } },
                        {
                            $project: {
                                image: '$variants.photos',
                            }
                        },
                    ]
                }
            }
        ]).exec();
        const size$ = result[0].images.map((obj: { image: string }) => {
            const url = obj.image.startsWith(this.configService.get(ENV.BASE_URL)) ? obj.image.replace(`${this.configService.get(ENV.BASE_URL)}/`, `http://127.0.0.1:3001/`) : obj.image;
            return this.http.head(url).pipe(map(res => parseInt(res.headers['content-length'], 10)));
        });
        return size$;
    }

    /**
     * Move the multer file to the right place (rename it and add uuidv4 to it)
     **/
    uploadImage(itemIdOrTmpCreate: string, file: Express.Multer.File): string {
        let extension = file.mimetype.replace('image/', '');
        extension = extension === 'jpeg' ? extension.replace('jpeg', 'jpg') : extension;
        const filename = `${itemIdOrTmpCreate}_${uuidv4()}.${extension}`;
        renameSync(file.path, join(this.configService.get(ENV.ASSETS_PATH), filename));
        this.markAsTmpImage(filename);
        return filename;
    }

    async uploadImageFromUrl(itemIdOrTmpCreate: string, url: string) {
        return lastValueFrom(this.http.get(url, { responseType: 'stream' }).pipe(
            switchMap(res => {
                const size = res.headers['content-length'];
                const type = res.headers['content-type'];
                if (!['image/jpeg', 'image/jpg', 'image/png', 'image/webp', 'image/gif'].includes(type)) {
                    throw new HttpException(`Unsupported image type ${type}`, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
                }
                let extension = type.replace('image/', '');
                extension = extension === 'jpeg' ? extension.replace('jpeg', 'jpg') : extension;
                const filename = `${itemIdOrTmpCreate}_${uuidv4()}.${extension}`;
                const filepath = join(this.configService.get(ENV.ASSETS_PATH), filename);
                const file = createWriteStream(filepath);
                const result = from(new Promise<void>((resolve, reject) => {
                    file.on('finish', () => resolve());
                    file.on('error', () => reject());
                }))
                res.data.pipe(file);
                let obs = result;
                if (size >= 300000 && ['image/jpeg', 'image/jpg', 'image/png'].includes(type)) {
                    obs = result.pipe(
                        switchMap(() => this.tinify.fromFile(filepath, filepath))
                    );
                }
                return obs.pipe(map(() => filename));
            }),
        ));
    }

    private markAsTmpImage(file: string) {
        this.tmpImages[file] = Date.now();
    }

    private removeTmpImage(file: string) {
        delete this.tmpImages[file];
    }

    private isTmpImage(file: string): boolean {
        return file in this.tmpImages;
    }

    private isOutdatedTmpImage(file: string): boolean {
        return this.tmpImages[file] + (3600000) >= Date.now();
    }

    private async handleTmpImages(item: ItemDocument) {
        // Rename tmp-create images.
        let shouldSave = false;
        for (const variant of item.variants) {
            for (const photo of variant.photos) {
                if (photo.includes('tmp-create')) {
                    const index = variant.photos.indexOf(photo);
                    const newFile = photo.replace(`tmp-create`, item._id);
                    const oldFilepath = join(
                        this.configService.get(ENV.ASSETS_PATH),
                        photo.replace(`${this.configService.get(ENV.BASE_URL)}/files/`, '')
                    );
                    const newFilepath = join(
                        this.configService.get(ENV.ASSETS_PATH),
                        newFile.replace(`${this.configService.get(ENV.BASE_URL)}/files/`, '')
                    );
                    renameSync(oldFilepath, newFilepath);
                    variant.photos[index] = newFile;
                    shouldSave = true;
                    const filename = basename(photo);
                    if (this.isTmpImage(filename)) {
                        this.removeTmpImage(filename);
                        this.markAsTmpImage(basename(newFile));
                    }
                }
            }
        }
        if (shouldSave) {
            item.markModified('variants');
            await item.save();
        }

        const obs = [];
        item.variants.forEach(variant => {
            variant.photos.forEach(photo => {
                const filename = basename(photo);
                if (this.isTmpImage(filename)) {
                    this.removeTmpImage(filename);
                    const file = join(this.configService.get(ENV.ASSETS_PATH), filename);
                    const stats = statSync(file);
                    if (stats.size > 300000) {
                        obs.push(this.tinify.fromFile(file, file));
                    }
                }
            });
        });
        if (obs.length > 0) {
            await lastValueFrom(zip(obs));
        }
        this.removeUnusedImagesItem(item);
    }

    private removeUnusedImagesItem(item: ItemDocument) {
        const photos = item.variants.reduce((acc, variant) => acc.concat(variant.photos), []);
        if (item.draft) {
            item.draft.variants.reduce((acc, variant) => acc.concat(variant.photos), []).forEach(photo => {
                photos.push(photo);
            });
        }
        const files = readdirSync(this.configService.get(ENV.ASSETS_PATH));
        files.filter(file => file.startsWith(item._id)).forEach(file => {
            if (!photos.find(photo => photo.includes(file))) {
                unlinkSync(join(this.configService.get(ENV.ASSETS_PATH), file));
                if (this.isTmpImage(file)) {
                    this.removeTmpImage(file);
                }
            }
        });
    }

    private updateNonMelImageOnError(obj: PreparedNonMelImage, err: Error) {
        let status = err instanceof HttpException ? err.getStatus() : HttpStatus.BAD_REQUEST;
        const url = new URL(obj.currentValue);
        url.searchParams.append('mel-error', status.toString());
        return from(this.itemModel.updateMany(
            { 'variants.photos': { $eq: obj.currentValue } },
            {
                $set: {
                    'variants.$.photos.$[p]': url.toString(),
                }
            },
            {
                arrayFilters: [
                    { p: obj.currentValue }
                ]
            },
        ));
    }

}