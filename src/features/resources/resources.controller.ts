import { Controller, Get, MessageEvent, SetMetadata, Sse, UseGuards } from "@nestjs/common";
import { concat, from, Observable, zip } from "rxjs";
import { delay, map } from "rxjs/operators";
import { AuthGuard } from "../keycloak/guards/auth.guard";
import { RolesGuard } from "../keycloak/guards/roles.guard";
import { BrandsService } from "./brands/brands.service";
import { CategoriesService } from "./categories/categories.service";
import { ColorsService } from "./colors/colors.service";
import { FeaturesService } from "./features/features.service";
import { ItemsService } from "./items/items.service";
import { UserContentsService } from "./user-contents/user-contents.service";

@Controller('api/resources')
export class ResourcesController {

    constructor(
        private brandsService: BrandsService,
        private colorsService: ColorsService,
        private categoriesService: CategoriesService,
        private featuresService: FeaturesService,
        private userContentsService: UserContentsService,
        private itemsService: ItemsService
    ) { }

    @Get('stats')
    @UseGuards(AuthGuard, RolesGuard)
    @SetMetadata('roles', ['dashboard-access', 'admin'])
    async stats(): Promise<any> {
        const ucStats: any[] = await this.userContentsService.stats();
        const iStats: any[] = await this.itemsService.stats();
        return iStats.concat(ucStats);
    }

    @Sse('dump')
    @UseGuards(AuthGuard, RolesGuard)
    @SetMetadata('roles', ['super-admin'])
    dump(): Observable<MessageEvent> {
        return concat(
            from(this.brandsService.findAll()).pipe(map(items => ({ items, type: 'brands', step: 1, total: 6 }))),
            from(this.colorsService.findAll()).pipe(map(items => ({ items, type: 'colors', step: 2, total: 6 }))),
            from(this.categoriesService.findAll()).pipe(map(items => ({ items, type: 'categories', step: 3, total: 6 }))),
            from(this.featuresService.findAll()).pipe(map(items => ({ items, type: 'features', step: 4, total: 6 }))),
            from(this.userContentsService.findAll()).pipe(map(items => ({ items, type: 'user-contents', step: 5, total: 6 }))),
            from(this.itemsService.findAll()).pipe(map(items => ({ items, type: 'items', step: 6, total: 6 }))),
        ).pipe(delay(100), map((data: any) => ({ data, id: data.step.toString(), retry: 0 } as MessageEvent)));
    }

    @Get('total-size')
    @UseGuards(AuthGuard, RolesGuard)
    @SetMetadata('roles', ['admin'])
    getTotalSize(): Observable<{ [key: string]: number }> {
        return this.totalSize();
    }

    private totalSize(): Observable<{ [key: string]: number }> {
        return zip(
            from(this.brandsService.totalSize()).pipe(map(size => ({ brands: size }))),
            from(this.colorsService.totalSize()).pipe(map(size => ({ colors: size }))),
            from(this.categoriesService.totalSize()).pipe(map(size => ({ categories: size }))),
            from(this.featuresService.totalSize()).pipe(map(size => ({ features: size }))),
            from(this.userContentsService.totalSize()).pipe(map(size => ({ userContents: size }))),
            from(this.itemsService.totalSize()).pipe(map(size => ({ items: size }))),
        ).pipe(
            map(values => values.reduce((acc, value) => {
                return { ...acc, ...value };
            }, {}))
        );
    }

}