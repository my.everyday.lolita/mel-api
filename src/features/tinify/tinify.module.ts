import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { TinifyService } from "./tinify.service";

@Module({
    imports: [ConfigModule],
    providers: [TinifyService],
    exports: [TinifyService],
})
export class TinifyModule { }