import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { from } from "rxjs";
import tinify from "tinify";
import { ENV } from "../environement/env.model";

@Injectable()
export class TinifyService {
    constructor(private configService: ConfigService) {
        tinify.key = this.configService.get(ENV.TINIFY_KEY);
    }

    fromFile(path: string, dest: string) {
        const source = tinify.fromFile(path);
        return from(source.toFile(dest));
    }

    fromBuffer(buffer: string, dest: string) {
        const source = tinify.fromBuffer(buffer);
        return from(source.toFile(dest));
    }

    fromUrl(url: string, dest: string) {
        const source = tinify.fromUrl(url);
        return from(source.toFile(dest));
    }

}