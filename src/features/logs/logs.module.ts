import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { KeycloakModule } from "../keycloak/keycloak.module";
import { AppLogger } from "./app.logger";
import { LogsController } from "./logs.controller";
import { Log, LogSchema } from "./logs.schema";
import { LogsService } from "./logs.service";

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Log.name, schema: LogSchema },
        ]),
        KeycloakModule,
    ],
    controllers: [
        LogsController
    ],
    providers: [
        AppLogger,
        LogsService,
    ],
    exports: [
        AppLogger
    ]
})
export class LogsModule { }