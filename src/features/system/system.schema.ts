import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type SystemInfosDocument = SystemInfos & Document;

export enum SystemStatus {
    MAINTENANCE = "maintenance",
    OK = "ok",
}

@Schema()
export class SystemInfos {
    @Prop({ required: true })
    status: SystemStatus;

    @Prop()
    estimatedDuration?: number;

    @Prop()
    startDate?: Date;
}

export const SystemInfosSchema = SchemaFactory.createForClass(SystemInfos);
