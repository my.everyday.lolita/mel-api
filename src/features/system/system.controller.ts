import { Body, Controller, Get, Put, SetMetadata, UseGuards } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { AuthGuard } from "../keycloak/guards/auth.guard";
import { RolesGuard } from "../keycloak/guards/roles.guard";
import { SystemInfos, SystemInfosDocument, SystemStatus } from "./system.schema";

@Controller('api/system')
export class SystemController {

    systemInfos: SystemInfosDocument;

    constructor(@InjectModel(SystemInfos.name) private model: Model<SystemInfosDocument>,) { }

    @Get()
    async getSystemInfo() {
        await this.getOrCreateSystemInfo();
        return {
            status: this.systemInfos.status,
            estimatedDuration: this.systemInfos.estimatedDuration,
            startDate: this.systemInfos.startDate,
        };
    }

    @Put()
    @UseGuards(AuthGuard, RolesGuard)
    @SetMetadata('roles', ['super-admin'])
    async updateSystemInfo(@Body() systemInfo: SystemInfos) {
        await this.getOrCreateSystemInfo();
        this.systemInfos.status = systemInfo.status;
        this.systemInfos.estimatedDuration = systemInfo.estimatedDuration;
        this.systemInfos.startDate = systemInfo.startDate;
        return this.systemInfos.save();
    }


    private async getOrCreateSystemInfo() {
        if (!this.systemInfos) {
            let document = await this.model.findOne().exec();
            if (!document) {
                document = new this.model({
                    status: SystemStatus.OK,
                });
                document.save();
            }
            this.systemInfos = document;
        }
    }
}